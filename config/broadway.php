<?php

return [
    'image' => [
        'thumb-dimensions' => [
            'product' => [
                'main_image' => [
                    [
                        'width' => 150,
                        'height' => 150
                    ],
                ],

                'gallery_image' => [
                    [
                        'width' => 50,
                        'height' => 50
                    ],
                    [
                        'width' => 150,
                        'height' => 150
                    ],
                ],

            ],

            'page' => [
                [
                    'width' => 100,
                    'height' => 100
                ]
            ],
        ],
    ],

];