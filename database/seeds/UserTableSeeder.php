<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'username' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('admin@123'),
            'first_name' => 'Broadway Admin',
            'last_name'=>'Admin',
            'middle_name'=>'',
            'status'=> 1,
        ]);
    }
}
