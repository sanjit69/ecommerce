@extends('admin.common.layout.layout')


    @section('content')

        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>

                @include('admin.user.partials.breadcumb')

            </div>

            <div class="page-content">
                <div class="page-header">
                    <h1>
                        {{ trans('admin/user/general.content.edit.edit') }}
                        <small>

                            <i class="icon-double-angle-right"></i>
                            {{ trans('admin/user/general.content.edit.edit_form') }}
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->

                        <form class="form-horizontal" role="form" method="post" action="{{ route('admin.users.update', ['id' =>$data['user']->id]) }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.username') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="username" value="{{ isset($data['user'])?$data['user']->username:'' }}" placeholder="Username" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.email') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="email" value="{{ isset($data['user'])?$data['user']->email:'' }}" placeholder="Email" class="col-xs-10 col-sm-5">
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.first_name') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="first_name" placeholder="First Name" value="{{ isset($data['user'])?$data['user']->first_name:'' }}" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.middle_name') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="middle_name" value="{{ isset($data['user'])?$data['user']->middle_name:'' }}" placeholder="Middle Name" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.last_name') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="last_name" placeholder="Last Name" value="{{ isset($data['user'])?$data['user']->last_name:'' }}" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.status') }} </label>

                                <div class="col-sm-9">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="control-group">

                                            <div class="radio">
                                                <label>
                                                    <input name="status" type="radio" class="ace" value="1" {{ isset($data['user']) &&  $data['user']->status== 1?'checked':'' }}>
                                                    <span class="lbl">{{ trans('admin/user/general.content.common.active') }}</span>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input name="status" type="radio" class="ace" value="0" {{ isset($data['user']) &&  $data['user']->status== 0?'checked':'' }}>
                                                    <span class="lbl"> {{ trans('admin/user/general.content.common.inactive') }}</span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>


                            </div>

                            <div class="space-4"></div>

                            <hr>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.password') }} </label>

                                <div class="col-sm-9">
                                    <input type="password" name="password" id="form-field-1" placeholder="Password" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.password_confirm') }} </label>

                                <div class="col-sm-9">
                                    <input type="password" name="passowrd_confirmation" id="form-field-1" placeholder="Confirm Password" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="icon-ok bigger-110"></i>
                                        Submit
                                    </button>

                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>


                        </form>

                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>


        @endsection