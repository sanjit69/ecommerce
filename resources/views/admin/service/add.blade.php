@extends('admin.common.layout.layout')


    @section('content')

        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>

                @include('admin.service.partials.breadcumb')

            </div>

            <div class="page-content">
                <div class="page-header">
                    <h1>
                        {{ trans('admin/service/general.content.common.manager') }}
                        <small>

                            <i class="icon-double-angle-right"></i>
                            {{ trans('admin/service/general.content.add.add_form') }}
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->

                        <form class="form-horizontal" role="form" method="post" action="{{ route('admin.services.store') }}" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" >
                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/service/general.content.common.title') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="title" placeholder="Service Title" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/service/general.content.common.sub_title') }} </label>

                                <div class="col-sm-9">
                                    <input type="text" id="form-field-1" name="sub_title" placeholder="Sub-Title" class="col-xs-10 col-sm-5">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right"
                                       for="form-field-1">Image </label>

                                <div class="col-sm-9">
                                    <input type="file" name="image">
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/service/general.content.common.description') }}</label>

                                <div class="col-sm-9">
                                    <textarea cols="50" rows="10" placeholder="Description" name="description">

                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/service/general.content.common.other_description') }}</label>

                                <div class="col-sm-9">
                                    <textarea cols="35" rows="5" placeholder="Description" name="other_description">

                                    </textarea>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.status') }} </label>

                                <div class="col-sm-9">
                                    <div class="col-xs-12 col-sm-6">
                                        <div class="control-group">

                                            <div class="radio">
                                                <label>
                                                    <input name="status" checked type="radio" class="ace" value="1">
                                                    <span class="lbl">{{ trans('admin/service/general.content.common.active') }}</span>
                                                </label>
                                            </div>

                                            <div class="radio">
                                                <label>
                                                    <input name="status" type="radio" class="ace" value="0">
                                                    <span class="lbl"> {{ trans('admin/service/general.content.common.inactive') }}</span>
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>



                            </div>






                            <div class="space-4"></div>


                            <div class="clearfix form-actions">
                                <div class="col-md-offset-3 col-md-9">
                                    <button class="btn btn-info" type="submit">
                                        <i class="icon-ok bigger-110"></i>
                                        Submit
                                    </button>

                                    &nbsp; &nbsp; &nbsp;
                                    <button class="btn" type="reset">
                                        <i class="icon-undo bigger-110"></i>
                                        Reset
                                    </button>
                                </div>
                            </div>


                        </form>

                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>


        @endsection