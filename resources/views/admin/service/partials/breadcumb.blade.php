<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
    </li>

    <li>
        <a href="{{ route('admin.users.index') }}">{{ trans('admin/service/general.menu.manager') }}</a>
    </li>



        <li class="active">{{ trans('admin/service/general.content.common.add') }}</li>


</ul><!-- .breadcrumb -->