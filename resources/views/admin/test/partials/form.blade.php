<form class="form-horizontal" role="form" method="post" action="{{ route('admin.menues.store') }}">
    <input type="hidden" name="_token" value="{{ csrf_token() }}" >
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/menu/general.content.common.title') }} </label>

        <div class="col-sm-9">
            <input type="text" id="form-field-1" name="title" placeholder="Please Enter Title" class="col-xs-10 col-sm-5">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/menu/general.content.common.key') }} </label>

        <div class="col-sm-9">
            <input type="text" id="form-field-1" name="key" placeholder="Please Enter Key" class="col-xs-10 col-sm-5">
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/menu/general.content.common.description') }} </label>

        <div class="col-sm-9">
            <textarea name="description" rows="10" cols="40"></textarea>
        </div>
    </div>

    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right" for="form-field-1"> {{ trans('admin/user/general.content.common.status') }} </label>

        <div class="col-sm-9">
            <div class="col-xs-12 col-sm-6">
                <div class="control-group">

                    <div class="radio">
                        <label>
                            <input name="status" type="radio" class="ace" value="1" >
                            <span class="lbl">{{ trans('admin/user/general.content.common.active') }}</span>
                        </label>
                    </div>

                    <div class="radio">
                        <label>
                            <input name="status" type="radio" class="ace" value="0">
                            <span class="lbl"> {{ trans('admin/user/general.content.common.inactive') }}</span>
                        </label>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <div class="space-4"></div>


    <div class="clearfix form-actions">
        <div class="col-md-offset-3 col-md-9">
            <button class="btn btn-info" type="submit">
                <i class="icon-ok bigger-110"></i>
                Submit
            </button>

            &nbsp; &nbsp; &nbsp;
            <button class="btn" type="reset">
                <i class="icon-undo bigger-110"></i>
                Reset
            </button>
        </div>
    </div>


</form>