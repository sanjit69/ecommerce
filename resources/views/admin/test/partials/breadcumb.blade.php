<ul class="breadcrumb">
    <li>
        <i class="icon-home home-icon"></i>
        <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
    </li>

    <li>
        <a href="{{ route('admin.menues.index') }}">{{ trans('admin/menu/general.content.common.manager') }}</a>
    </li>

    @if (isset($data))
        <li class="active">{{ trans('admin/menu/general.content.edit.edit') }}</li>
        @else

        <li class="active">{{ trans('admin/menu/general.content.common.add') }}</li>
        @endif

</ul><!-- .breadcrumb -->