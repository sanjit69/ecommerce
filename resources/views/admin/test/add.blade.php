@extends('admin.common.layout.layout')


    @section('content')

        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try{ace.settings.check('breadcrumbs' , 'fixed')}catch(e){}
                </script>

                @include('admin.menu.partials.breadcumb')

            </div>

            <div class="page-content">
                <div class="page-header">
                    <h1>
                        {{ trans('admin/menu/general.content.common.manager') }}
                        <small>

                            <i class="icon-double-angle-right"></i>
                            {{ trans('admin/menu/general.content.add.add_form') }}
                        </small>
                    </h1>
                </div><!-- /.page-header -->

                <div class="row">
                    <div class="col-xs-12">
                        <!-- PAGE CONTENT BEGINS -->

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @include('admin.menu.partials.form')

                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div>


        @endsection