@extends('admin.common.layout.layout')

@section('page_title') {{ trans($trans_path.'content.common.manager') }} | {{ trans($trans_path.'content.list.list') }} - {{ trans('general.admin_panal') }} @endsection

@section('content')

    <div class="main-content">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>

            <ul class="breadcrumb">
                <li>
                    <i class="icon-home home-icon"></i>
                    <a href="{{ route('admin.dashboard') }}">{{ trans('general.home') }}</a>
                </li>

                <li>
                    <a href="{{ route('admin.pages.index') }}">{{ trans($trans_path.'content.common.manager') }}</a>
                </li>
                <li class="active"> {{ trans($trans_path.'content.list.list') }} </li>
            </ul><!-- .breadcrumb -->

            <div class="nav-search" id="nav-search">
                <form class="form-search">
								<span class="input-icon">
									<input type="text" placeholder="Search ..." class="nav-search-input"
                                           id="nav-search-input" autocomplete="off">
									<i class="icon-search nav-search-icon"></i>
								</span>
                </form>
            </div><!-- #nav-search -->
        </div>

        <div class="page-content">
            <div class="page-header">
                <h1>

                    {{ trans($trans_path.'content.common.manager') }}
                    <small>
                        <i class="icon-double-angle-right"></i>
                        {{ trans($trans_path.'content.list.product_list') }}
                    </small>
                </h1>
            </div><!-- /.page-header -->

            <div class="row">
                <div class="col-xs-12">
                    <!-- PAGE CONTENT BEGINS -->

                    @if (Request::session()->has('message'))

                        <div class="alert alert-info">
                            <button type="button" class="close" data-dismiss="alert">
                                <i class="icon-remove"></i>
                            </button>
                            {!! Request::session()->get('message') !!}
                        </div>

                    @endif
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="table-responsive">
                                <table id="sample-table-1" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <th class="center">&nbsp;</th>
                                        <th>{{ trans($trans_path.'content.common.sn') }}</th>
                                        <th>{{ trans($trans_path.'content.common.name') }}</th>
                                        <th>{{ trans($trans_path.'content.common.image') }}</th>
                                        <th class="hidden-480">{{ trans($trans_path.'content.common.category') }}</th>
                                        <th>{{ trans($trans_path.'content.common.old_price') }}</th>
                                        <th>{{ trans($trans_path.'content.common.new_price') }}</th>
                                        <th>{{ trans($trans_path.'content.common.quantity') }}</th>
                                        <th class="hidden-480">{{ trans($trans_path.'content.common.status') }}</th>
                                        <th class="hidden-480">Is Featuted</th>

                                        <th>{{ trans($trans_path.'content.common.operation') }}</th>
                                    </tr>
                                    <tr>
                                        <th class="center">&nbsp;</th>
                                        <th>&nbsp;</th>
                                        <th><input type="text" name="product_title" id="product_title"></th>
                                        <th>&nbsp;</th>
                                        <th><input type="text" name="product_category" id="product_category"></th>
                                        <th>
                                            <input type="text" name="product_old_price_start" id="product_old_price_start">
                                            <input type="text" name="product_old_price_end" id="product_old_price_end">
                                        </th>
                                        <th>
                                            <input type="text" name="product_old_price_start" id="product_new_price_start">
                                            <input type="text" name="product_old_price_end" id="product_new_price_end">
                                        </th>
                                        <th><input type="number" name="product_remaining_quantity" id="product_remaining_quantity"></th>
                                        <th>
                                            <select name="product_status" id="product_status">
                                                <option value="all">All</option>
                                                <option value="active">Active</option>
                                                <option value="inactive">In-active</option>
                                            </select>
                                        </th>
                                        <th><input type="number" name="product_is_featured" id="product_is_featured"></th>

                                        <th><button class="btn btn-xm btn-success" id="product_search_btn">Search</button></th>
                                    </tr>
                                    </thead>

                                    <tbody>

                                    @if ($data['rows']->count() > 0)

                                        @foreach($data['rows'] as $row)

                                            <tr>
                                                <td class="center">
                                                    <label>
                                                        <input type="checkbox" class="ace">
                                                        <span class="lbl"></span>
                                                    </label>
                                                </td>
                                                <td>
                                                    {{ $row->id }}
                                                </td>
                                                <td>
                                                    <a href="#">{{ $row->name }}</a>
                                                </td>
                                                <td>@if ($row->main_image)
                                                        <img src="{{ asset('images/product/'.$row->main_image ) }}"
                                                             alt="" width="50px">
                                                    @else
                                                        <p>No Banner Uploaded.</p>

                                                    @endif</td>
                                                <td>{{ $row->category_title }}</td>
                                                <td>{{ $row->old_price }}</td>
                                                <td>{{ $row->new_price }}</td>
                                                <td class="hidden-480">{{ $row->quantity }}</td>
                                                <td class="hidden-480">
                                                    @if ($row->status ==1)
                                                        <span class="label label-sm label-success">Active</span>
                                                    @else
                                                        <span class="label label-sm label-info">In-Active</span>
                                                    @endif
                                                </td>

                                                <td>
                                                    <div class="visible-md visible-lg hidden-sm hidden-xs btn-group">


                                                        <a class="btn btn-xs btn-info"
                                                           href="{{ route($base_route.'.edit', ['id' => $row->id]) }}">
                                                            <i class="icon-edit bigger-120"></i>
                                                        </a>

                                                        <a class="btn btn-xs bootbox-confirm btn-danger"
                                                           href="{{ route($base_route.'.delete', ['id' => $row->id]) }}">
                                                            <i class="icon-trash bigger-120"></i>
                                                        </a>


                                                    </div>

                                                    <div class="visible-xs visible-sm hidden-md hidden-lg">
                                                        <div class="inline position-relative">
                                                            <button class="btn btn-minier btn-primary dropdown-toggle"
                                                                    data-toggle="dropdown">
                                                                <i class="icon-cog icon-only bigger-110"></i>
                                                            </button>

                                                            <ul class="dropdown-menu dropdown-only-icon dropdown-yellow pull-right dropdown-caret dropdown-close">
                                                                <li>
                                                                    <a href="#" class="tooltip-info" data-rel="tooltip"
                                                                       title="" data-original-title="View">
																				<span class="blue">
																					<i class="icon-zoom-in bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-success"
                                                                       data-rel="tooltip" title=""
                                                                       data-original-title="Edit">
																				<span class="green">
																					<i class="icon-edit bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>

                                                                <li>
                                                                    <a href="#" class="tooltip-error" data-rel="tooltip"
                                                                       title="" data-original-title="Delete">
																				<span class="red">
																					<i class="icon-trash bigger-120"></i>
																				</span>
                                                                    </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>

                                        @endforeach

                                    @else
                                        <tr>
                                            <td colspan="11">No Data Found</td>
                                        </tr>


                                    @endif
                                    </tbody>
                                </table>
                            </div><!-- /.table-responsive -->
                        </div><!-- /span -->
                    </div><!-- /row -->

                    <div class="hr hr-18 dotted hr-double"></div>


                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.page-content -->
    </div>

@endsection


@section('page_specific_scripts')

    <script src="{{ asset('assets/admin/js/bootbox.min.js')  }}"></script>

    <script>
        $(".bootbox-confirm").on(ace.click_event, function () {
            bootbox.confirm("Are you sure?", function (result) {
                if (result) {
                    //
                }
            });
        });

    </script>

    <script>
        $(document).ready(function () {
            
            
            //search
            $('#product_search_btn').click(function () {

                var title = $('#product_title').val();
                var category = $('#product_category').val();
                var old_price_start = $('#product_old_price_start').val();
                var old_price_end = $('#product_old_price_end').val();
                var new_price_start = $('#product_new_price_start').val();
                var new_price_end = $('#product_new_price_end').val();
                var remaining_quantity = $('#product_remaining_quantity').val();
                var status = $('#product_status').val();

                var url = '{{ route('admin.products.index') }}';

                //url = url + '?title=' + title + '&category=' + category;

                var url_int = false;
                if(title != "") {
                    url = url + '?title=' + title;
                    url_int = true;
                }

                if(category != "") {
                    if(url_int) {
                        url = url + '&category=' + category;
                    }else {
                        url = url + '?category=' + category;
                        url_int = true;
                    }
                }

                if(old_price_start != "") {
                    if(url_int) {
                        url = url + '&old-price-start=' + old_price_start;
                    }else {
                        url = url + '?old-price-start=' + old_price_start;
                        url_int = true;
                    }
                }

                if(old_price_end != "") {
                    if(url_int) {
                        url = url + '&old-price-end=' + old_price_end;
                    }else {
                        url = url + '?old-price-end=' + old_price_end;
                        url_int = true;
                    }
                }

                if(new_price_start != "") {
                    if(url_int) {
                        url = url + '&new-price-start=' + new_price_start;
                    }else {
                        url = url + '?new-price-start=' + new_price_start;
                        url_int = true;
                    }
                }
                if(new_price_end != "") {
                    if(url_int) {
                        url = url + '&new-price-end=' + new_price_end;
                    }else {
                        url = url + '?new-price-end=' + new_price_end;
                        url_int = true;
                    }
                }

                if(remaining_quantity != "") {
                    if(url_int) {
                        url = url + '&qty=' + remaining_quantity;
                    }else {
                        url = url + '?qty=' + remaining_quantity;
                        url_int = true;
                    }
                }

                if(status != "") {
                    if(url_int) {
                        url = url + '&status=' + status;
                    }else {
                        url = url + '?status=' + status;
                    }
                }

                console.log(url);
                return false;
                location.href = url;
                return false;
            });
        });
    </script>


@endsection