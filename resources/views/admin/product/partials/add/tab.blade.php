<div class="tabbable tabs-left">
    <ul class="nav nav-tabs" id="myTab3">
        <li class="active">
            <a data-toggle="tab" href="#general">
                <i class="pink icon-dashboard bigger-110"></i>
                General
            </a>
        </li>

        <li class="">
            <a data-toggle="tab" href="#images">
                <i class="blue icon-picture bigger-110"></i>
                Images
            </a>
        </li>

        <li class="">
            <a data-toggle="tab" href="#attributes">
                <i class="blue icon-rocket bigger-110"></i>
                Attributes
            </a>
        </li>

        <li class="">
            <a data-toggle="tab" href="#tag">
                <i class="blue icon-tags bigger-110"></i>
                Tag
            </a>
        </li>

        <li>
            <a data-toggle="tab" href="#seo">
                <i class="icon-rocket"></i>
                SEO
            </a>
        </li>
    </ul>

   @include($view_path.'partials.add.tab-content')

</div>