<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.seo_title') }} </label>

    <div class="col-sm-9">
        <input type="text" name="seo_title" value="" id="seo_title" placeholder="SEO title" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.seo_desc') }} </label>

    <div class="col-sm-9">
        <textarea name="seo_desc" id="seo_desc" cols="40" rows="10">{{ ViewHelper::getData('seo_desc', isset($data['row'])?$data['row']:[]) }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.seo_keyword') }} </label>

    <div class="col-sm-9">
        <input type="text" name="seo_keyword" value="" id="seo_keyword" placeholder="SEO Keyword" class="col-xs-10 col-sm-5">
    </div>
</div>