<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.category') }} </label>

    <div class="col-sm-9">
        <select name="category_id" class="col-sm-5">
            @if ($data['categories']->count() > 0)
                @foreach($data['categories'] as $category)



                    <option value="{{ $category->id }}">{{ $category->title }}</option>
                @endforeach
            @else
                <option value="0">No Menu Available</option>
            @endif
        </select>
    </div>
</div>


<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.name') }} </label>

    <div class="col-sm-9">
        <input type="text" required name="name" value="" id="name" placeholder="Name" class="col-xs-10 col-sm-12">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.old_price') }} </label>

    <div class="col-sm-9">
        <input type="text" name="old_price" value="" id="old_price" placeholder="Old Price" class="col-xs-10 col-sm-5">
    </div>
</div>
<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.new_price') }} </label>

    <div class="col-sm-9">
        <input type="text" name="new_price" value="" id="new_price" placeholder="New Price" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.quantity') }} </label>

    <div class="col-sm-9">
        <input type="number" min="0" max="100" name="quantity" value="" id="quantity" placeholder="Only Number is valid" class="col-xs-10 col-sm-5">
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.short_desc') }} </label>

    <div class="col-sm-9">
        <textarea name="short_desc" placeholder="Short-Description" id="short_desc" cols="40" rows="10">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
    </div>
</div>

<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.long_desc') }} </label>

    <div class="col-sm-9">
        <textarea required name="long_desc" placeholder="Long-Description" id="long_desc" cols="40" rows="10">{{ ViewHelper::getData('description', isset($data['row'])?$data['row']:[]) }}</textarea>
    </div>
</div>



<div class="form-group">
    <label class="col-sm-3 control-label no-padding-right"
           for="form-field-1"> {{ trans($trans_path.'content.common.status') }} </label>

    <div class="col-sm-9">
        <div class="col-xs-12 col-sm-6">
            <div class="control-group">

                @php
                    $active = 'checked';
                    $inactive = '';

                    if(old('status')){
                        if(old('status') == 0){
                            $inactive = 'checked';
                            $active = '';
                        }
                    }
                    elseif (isset($data['row'])) {
                        if ($data['row']->status == 0) {
                            $inactive = 'checked';
                            $active = '';
                        }
                    }

                @endphp
                <div class="radio">
                    <label>
                        <input name="status" {{ $active }} type="radio" class="ace" value="1">
                        <span class="lbl">{{ trans($trans_path.'content.common.active') }}</span>
                    </label>
                </div>

                <div class="radio">
                    <label>
                        <input name="status" {{ $inactive }} type="radio" class="ace" value="0">
                        <span class="lbl"> {{ trans($trans_path.'content.common.inactive') }}</span>
                    </label>
                </div>


            </div>
        </div>
    </div>


</div>


<div class="space-4"></div>