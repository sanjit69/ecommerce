<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Creative Link : Graphic Design, Press &amp; Stamp Bank</title>
    <meta name="description" content=" " />
    <link href="{{ asset('assets/frontend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/favicon.ico') }}" rel="shortcut icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body>
<div id="header">
    <div class="headerTop">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul>
                        <li><i class="fa fa-map-marker"></i> 102 Machhigalli, Bagbazar, Kathmandu</li>
                        <li><i class="fa fa-envelope"></i> creativelink@gmail.com</li>
                        <li style="display:none;"><i class="fa fa-phone"></i> +977-1-4216913, 4247765</li>
                    </ul>
                    <div class="fbTop"><a href="https://www.facebook.com/creativelinknepal/" target="_blank"><span><i class="fa fa-facebook"></i></span> Find Us On Facebook</a></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="logoSection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="logo bounceInLeft animated">
                        <div class="logoBlock"><a href="/"><img src="{{ asset('assets/frontend/images/Creative-Logo.png') }}" alt="Creative Link" /></a></div>
                        <div class="gifBox"><img src="{{ asset('assets/frontend/images/gif-image.gif') }}" alt="Creative Link" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="phoneRight"> <span><i class="fa fa-phone"></i></span>
                        <div class="text">4216913 <br />
                            4247765</div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">

            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="/">Navigation &raquo;</a> </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/">Home</a></li>
                            <li><a href="our-services.php">Our Services</a></li>
                            <li><a href="products.php">Products</a></li>
                            <li><a href="request-quotation.php">Request Quotation</a></li>
                            <li><a href="about-us.php">About Us</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                            <li><a href="inquiry-form.php">Inquiry Form</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
                <!--/.container-fluid -->
            </nav>
        </div>
    </div>

</div>

<div class="slider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="outerlayer">
                    <div class="innerlayer">
                        <ul class="bxslider">
                            <li><img src="{{ asset('assets/frontend/slider/slider1.jpg') }}" alt="Slider First" title="Slider First" /></li>
                            <li><img src="{{ asset('assets/frontend/slider/slider2.jpg') }}" alt="Slider Second" title="Slider Second" /></li>
                            <li><img src="{{ asset('assets/frontend/slider/slider3.jpg') }}" alt="Slider Third" title="Slider Third" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fourOpts">
    <div class="container">
        <div class="row">
            <ul class="clearfix">
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/thumbs.png') }}" alt="Quality Printing" /></span>
                    <h3>Quality Printing</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/on-time.png') }}" alt="Timely Delivery" /></span>
                    <h3>Timely Delivery</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/working-hrs.png') }}" alt="Flexible Working Hrs" /></span>
                    <h3>Flexible Working Hrs</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/support.png') }}" alt="Phone / Email Support" /></span>
                    <h3>Phone / Email Support</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="svcHome">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Our Services</h2>
            </div>
            <div class="clear"></div>
            <ul class="clearfix">
                <li> <img src="{{ asset('assets/frontend/images/graphics-design.jpg') }}" alt="Graphics Design" />
                    <h3><a href="graphics-design.php">Graphics Design</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/offset-printing.jpg') }}" alt="Offset Printing" />
                    <h3><a href="offset-printing.php">Offset Printing</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/screen-printing.jpg') }}" alt="Screen Printing" />
                    <h3><a href="screen-printing.php">Screen Printing</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/stamp.jpg') }}" alt="Stamp Bank" />
                    <h3><a href="stamp-bank.php">Stamp Bank</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/flex-printing.jpg') }}" alt="Flex Printing" />
                    <h3><a href="flex-printing.php">Flex Printing</a></h3>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="welcomeHome">
    <div class="overlayBox">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Welcome to Creative Link Pvt. Ltd.</h1>
                    <p>It is our great pleasure to introduce; Creative Link Pvt. Ltd. has been established primarily to serve the NATION in the field of Graphic Design, Press and a Stamp Bank.</p>
                    <p>It has been providing its services at affordable price in timely manner. Moreover, quality is the key factor it provides than the competitor organizations so that it has gained popularity since the period of its establishment.</p>
                    <p>The company is registered dated back on 2058/01/04 in the Company Registrar Company and the registration number is 15450/057-58. Also, the company has registered in cottage and small industries for initiation of its own Offset Printing Press on 2066/07/17 and the registration number is 11593/126. Our VAT number is 300461302.</p>
                    <p>It's our great opportunity to inform all of our customers and well-wishers that we are launching our own Offset Printing Press. We also promise for our best quality services to all of our customers: The best you choose, the best you perform !</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolioHome">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Our Sample Works</h2>
            </div>
            <ul class="clearfix">
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
            </ul>
        </div>
    </div>
</div>

<div id="footer">
    <div class="footerTop">
        <h2>Creative Link Pvt. Ltd.</h2>
        <h3>Graphic Design, Press &amp; Stamp Bank</h3>
        <p>Address: 102 Machhigalli, Bagbazar, Kathmandu, Nepal<br />
            Phone: +977-1-4216913, 4247765<br />
            Email: creativelink@gmail.com</p>
    </div>
    <div class="footerBottom">Copyright &copy; Creative Link Pvt. Ltd. - <?php echo date('Y'); ?> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Design By: <a href="https://www.broadwayinfosys.com/" target="_blank">Broadway Infosys</a></div>
    <!-- External Files -->
    <script src="assets/frontend/js/jquery-3.1.0.min.js"></script>
    <script src="assets/frontend/bootstrap/js/bootstrap.min.js"></script>
    <!-- Font Awesome -->
    <link href="assets/frontend/FontAwesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/frontend/css/animate.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i|Oswald:300,400" rel="stylesheet">

</div>
<!-- BX Slider -->
<script src="{{ asset('assets/frontend/bxslider/jquery.bxslider.js') }}"></script>
<link href="{{ asset('assets/frontend/bxslider/jquery.bxslider.css') }}" rel="stylesheet" />
<script>
    $(document).ready(function(){
        $('.bxslider').bxSlider();
    });
</script>
<!-- Fancybox -->
<script src="{{ asset('assets/frontend/Fancybox/jquery.fancybox.pack.js') }}"></script>
<link href="{{ asset('assets/frontend/Fancybox/jquery.fancybox.css') }}" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
</body>
</html>
