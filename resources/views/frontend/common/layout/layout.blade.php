<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>Creative Link : Graphic Design, Press &amp; Stamp Bank</title>
    <meta name="description" content=" " />
    <link href="{{ asset('assets/frontend/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/style.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/favicon.ico') }}" rel="shortcut icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
</head>

<body>
<!-----------this is menu-------->
@include('frontend.common.menu')

<!-----------this is slider---------->
@include('frontend.common.slider')

<!-------------------frontend content start here---------------->
@yield('content')
@include('frontend.common.content');

<!---------content end here------------>

<div id="footer">
    <div class="footerTop">
        <h2>Creative Link Pvt. Ltd.</h2>
        <h3>Graphic Design, Press &amp; Stamp Bank</h3>
        <p>Address: 102 Machhigalli, Bagbazar, Kathmandu, Nepal<br />
            Phone: +977-1-4216913, 4247765<br />
            Email: creativelink@gmail.com</p>
    </div>
    <div class="footerBottom">Copyright &copy; Creative Link Pvt. Ltd. - <?php echo date('Y'); ?> &nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;&nbsp; Design By: <a href="https://www.broadwayinfosys.com/" target="_blank">Broadway Infosys</a></div>
    <!-- External Files -->
    <script src="assets/frontend/js/jquery-3.1.0.min.js"></script>
    <script src="assets/frontend/bootstrap/js/bootstrap.min.js"></script>
    <!-- Font Awesome -->
    <link href="assets/frontend/FontAwesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="assets/frontend/css/animate.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,400i,700,700i|Oswald:300,400" rel="stylesheet">

</div>
<!-- BX Slider -->
<script src="{{ asset('assets/frontend/bxslider/jquery.bxslider.js') }}"></script>
<link href="{{ asset('assets/frontend/bxslider/jquery.bxslider.css') }}" rel="stylesheet" />
<script>
    $(document).ready(function(){
        $('.bxslider').bxSlider();
    });
</script>
<!-- Fancybox -->
<script src="{{ asset('assets/frontend/Fancybox/jquery.fancybox.pack.js') }}"></script>
<link href="{{ asset('assets/frontend/Fancybox/jquery.fancybox.css') }}" rel="stylesheet" />
<script type="text/javascript">
    $(document).ready(function() {
        $(".fancybox").fancybox();
    });
</script>
</body>
</html>
