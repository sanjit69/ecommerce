<div class="fourOpts">
    <div class="container">
        <div class="row">
            <ul class="clearfix">
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/thumbs.png') }}" alt="Quality Printing" /></span>
                    <h3>Quality Printing</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/on-time.png') }}" alt="Timely Delivery" /></span>
                    <h3>Timely Delivery</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/working-hrs.png') }}" alt="Flexible Working Hrs" /></span>
                    <h3>Flexible Working Hrs</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
                <li class="col-lg-3 col-sm-6"> <span><img src="{{ asset('assets/frontend/images/support.png') }}" alt="Phone / Email Support" /></span>
                    <h3>Phone / Email Support</h3>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley.</p>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="svcHome">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Our Services</h2>
            </div>
            <div class="clear"></div>
            <ul class="clearfix">
                <li> <img src="{{ asset('assets/frontend/images/graphics-design.jpg') }}" alt="Graphics Design" />
                    <h3><a href="graphics-design.php">Graphics Design</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/offset-printing.jpg') }}" alt="Offset Printing" />
                    <h3><a href="offset-printing.php">Offset Printing</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/screen-printing.jpg') }}" alt="Screen Printing" />
                    <h3><a href="screen-printing.php">Screen Printing</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/stamp.jpg') }}" alt="Stamp Bank" />
                    <h3><a href="stamp-bank.php">Stamp Bank</a></h3>
                </li>
                <li> <img src="{{ asset('assets/frontend/images/flex-printing.jpg') }}" alt="Flex Printing" />
                    <h3><a href="flex-printing.php">Flex Printing</a></h3>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="welcomeHome">
    <div class="overlayBox">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1>Welcome to Creative Link Pvt. Ltd.</h1>
                    <p>It is our great pleasure to introduce; Creative Link Pvt. Ltd. has been established primarily to serve the NATION in the field of Graphic Design, Press and a Stamp Bank.</p>
                    <p>It has been providing its services at affordable price in timely manner. Moreover, quality is the key factor it provides than the competitor organizations so that it has gained popularity since the period of its establishment.</p>
                    <p>The company is registered dated back on 2058/01/04 in the Company Registrar Company and the registration number is 15450/057-58. Also, the company has registered in cottage and small industries for initiation of its own Offset Printing Press on 2066/07/17 and the registration number is 11593/126. Our VAT number is 300461302.</p>
                    <p>It's our great opportunity to inform all of our customers and well-wishers that we are launching our own Offset Printing Press. We also promise for our best quality services to all of our customers: The best you choose, the best you perform !</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="portfolioHome">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h2>Our Sample Works</h2>
            </div>
            <ul class="clearfix">
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
                <li><a href="{{ asset('assets/frontend/Business-Photos/sample.jpg') }}" class="fancybox" title="Sample Work" rel="SamWork"><img src="Business-Photos/sample.jpg" alt="Sample Work" /></a></li>
            </ul>
        </div>
    </div>
</div>