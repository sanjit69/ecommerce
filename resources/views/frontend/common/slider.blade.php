<div class="slider">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="outerlayer">
                    <div class="innerlayer">
                        <ul class="bxslider">
                            <li><img src="{{ asset('assets/frontend/slider/slider1.jpg') }}" alt="Slider First" title="Slider First" /></li>
                            <li><img src="{{ asset('assets/frontend/slider/slider2.jpg') }}" alt="Slider Second" title="Slider Second" /></li>
                            <li><img src="{{ asset('assets/frontend/slider/slider3.jpg') }}" alt="Slider Third" title="Slider Third" /></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>