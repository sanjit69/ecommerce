<div id="header">

    <div class="headerTop">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <ul>
                        <li><i class="fa fa-map-marker"></i> 102 Machhigalli, Bagbazar, Kathmandu</li>
                        <li><i class="fa fa-envelope"></i> creativelink@gmail.com</li>
                        <li style="display:none;"><i class="fa fa-phone"></i> +977-1-4216913, 4247765</li>
                    </ul>
                    <div class="fbTop"><a href="https://www.facebook.com/creativelinknepal/" target="_blank"><span><i class="fa fa-facebook"></i></span> Find Us On Facebook</a></div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="logoSection">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="logo bounceInLeft animated">
                        <div class="logoBlock"><a href="/"><img src="{{ asset('assets/frontend/images/Creative-Logo.png') }}" alt="Creative Link" /></a></div>
                        <div class="gifBox"><img src="{{ asset('assets/frontend/images/gif-image.gif') }}" alt="Creative Link" /></div>
                        <div class="clear"></div>
                    </div>
                    <div class="phoneRight"> <span><i class="fa fa-phone"></i></span>
                        <div class="text">4216913 <br />
                            4247765</div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="menu">
        <div class="container">

            <!-- Static navbar -->
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                        <a class="navbar-brand" href="/">Navigation &raquo;</a> </div>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="/">Home</a></li>
                            <li><a href="our-services.php">Our Services</a></li>
                            <li><a href="products.php">Products</a></li>
                            <li><a href="request-quotation.php">Request Quotation</a></li>
                            <li><a href="about-us.php">About Us</a></li>
                            <li><a href="contact-us.php">Contact Us</a></li>
                            <li><a href="inquiry-form.php">Inquiry Form</a></li>
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
                <!--/.container-fluid -->
            </nav>
        </div>
    </div>

</div>