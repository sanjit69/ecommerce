<?php

return [
    'admin_panel' => 'Admin Panal',

    'home'=>'Home ',
    'user_name'=>'User Name',

    'button' => [
        'submit' => 'Submit',
        'update' => 'Update',
        'reset' => 'Reset',
        'delete' => 'Delete',
     ],
];