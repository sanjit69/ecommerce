<?php

return [
    'message'=> [

    ],
    'menu'=> [
        'user_manager' => 'User Manager',
        'list' => 'List',
        'add' => 'Add',
    ],

    'button'=> [

    ],

    'content'=> [
        'common'=>[
            'add'=>'Add',
            'active'=>'Active',
            'created_at'=>'Created At',
            'email'=>'Email',
            'first_name'=>'First Name',
            'inactive'=>'Inactive',
            'last_name'=>'Last name',
            'middle_name'=>'Middle Name',
            'name'=>'User Name',
            'password'=>'Password',
            'password_confirm'=>'Confirmed Password',
            'operation'=>'Operations',
            'status'=>'Status',
            'updated_at'=>'Updated At',
            'user_manager'  => 'User Manager',
            'username'=>'User Name',
        ],
        'list'=>[
            'list'=>'List',
            'user_list'=>'User List',
        ],
        'add'=>[
            'add_form'=>'Add Form',
            'edit_form' => 'Edit Form',
        ],
        'edit'=>[
        'edit' => 'Edit',
        'edit_form' => 'Edit Form',
        ],
        'delete'=>[],
    ],

];