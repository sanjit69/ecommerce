<?php

return [
    'message'=> [

    ],
    'menu'=> [
        'manager' => 'Page Manager',
        'list' => 'List',
        'add' => 'Add',
    ],

    'button'=> [

    ],

    'content'=> [
            'common'=>[
            'add'=> 'Add',
            'active' =>'Active',
            'inactive' => 'In-active',
            'banner'=>'Banner',
            'description' =>'Description',
            'title' => 'Title',
            'menu' => 'Menu',
            'created_at'=>'Created At',
            'updated_at'=>'Updated At',
            'status'=>'Status',
            'operation'=>'Operation',
            'manager'=>'Page Manager'

        ],
        'list'=>[
            'list'=>'List',
            'page_list'=>'Page List',
        ],
        'add'=>[
            'add_form'=>'Add Form',
        ],
        'edit'=>[
        'edit' => 'Edit',
        'edit_form' => 'Edit Form',
        'existing_banner' => 'Existing Banner',
        'banner'=>'Banner',
        ],
        'delete'=>[],
    ],

];