<?php

return [
    'message'=> [

    ],
    'menu'=> [
        'manager' => 'Product Manager',
        'list' => 'List',
        'add' => 'Add',
    ],

    'button'=> [

    ],

    'content'=> [
            'common'=>[
            'add'=> 'Add',
            'active' =>'Active',
            'category'=>'Category',
            'inactive' => 'In-active',
            'image' => 'Image',
            'description' =>'Description',
            'long_desc' => 'Long Description',
            'name' => 'Name',
            'created_at'=>'Created At',
            'updated_at'=>'Updated At',
            'operation'=>'Operation',
            'old_price'=>'Old Price',
            'manager'=>'Product Manager',
            'main_image'=> 'Main Image',
            'new_price' => 'New Price',
            'quantity' => 'Quantity',
            'status'=>'Status',
            'sn' => 'SN',
            'short_desc'=>'Short Description',
            'status' => 'Status',
            'seo_title' => 'SEO Title',
            'seo_desc' => 'SEO Description',
            'seo_keyword' => 'SEO Keyword',


        ],
        'list'=>[
            'list'=>'List',
            'product_list'=>'Product List',
        ],
        'add'=>[
            'add_form'=>'Add Form',
        ],
        'edit'=>[
        'edit' => 'Edit',
        'edit_form' => 'Edit Form',
        'main_image'=>'Main Image',
        ],
        'delete'=>[],
    ],

];