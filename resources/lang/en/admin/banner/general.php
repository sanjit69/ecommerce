<?php

return [
    'message'=> [

    ],
    'menu'=> [
        'manager' => 'Banner Manager',
        'list' => 'List',
        'add' => 'Add',
    ],

    'button'=> [

    ],

    'content'=> [
            'common'=>[
            'add'=> 'Add',
            'active' =>'Active',
            'inactive' => 'In-active',
            'banner'=>'Banner',
            'description' =>'Description',
            'other_description' => 'Other Description',
            'title' => 'Title',
            'menu' => 'Menu',
            'sub_title' =>'Sub Title',
            'created_at'=>'Created At',
            'created_by'=>'Created By',
            'updated_at'=>'Updated At',
            'status'=>'Status',
            'operation'=>'Operation',
            'manager'=>'Banner',

        ],
        'list'=>[
            'list'=>'List',
            'page_list'=>'Page List',
            'service_list' => 'Service List',
        ],
        'add'=>[
            'add_form'=>'Add Form',
        ],
        'edit'=>[
        'edit' => 'Edit',
        'edit_form' => 'Edit Form',
        'existing_banner' => 'Existing Banner',
        'banner'=>'Banner',
        ],
        'delete'=>[],
    ],

];