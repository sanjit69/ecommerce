<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('frontend.common.layout.layout');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/logout', 'Auth\LoginController@index');


Route::get('admin/dashboard',               ['as' => 'admin.dashboard',         'uses' => 'Admin\DashboardController@index']);
Route::get('admin/error/{code}',            ['as' => 'admin.error',             'uses' => 'Admin\DashboardController@error']);

Route::get('admin/user',                    ['as' => 'admin.users.index',       'uses' => 'Admin\UserController@index']);
Route::get('admin/user/add',                ['as' => 'admin.users.add',         'uses' => 'Admin\UserController@add']);
Route::post('admin/user/store',             ['as' => 'admin.users.store',       'uses' => 'Admin\UserController@store']);
Route::get('admin/user/{user_id}/edit',     ['as' => 'admin.users.edit',        'uses' => 'Admin\UserController@edit']);
Route::post('admin/user/{id}/update',       ['as' => 'admin.users.update',      'uses' => 'Admin\UserController@update']);
Route::get('admin/user/{id}/delete',        ['as' => 'admin.users.delete',      'uses' => 'Admin\UserController@delete']);


Route::get('admin/pages',                    ['as' => 'admin.pages.index',       'uses' => 'Admin\PageController@index']);
Route::get('admin/page/add',                 ['as' => 'admin.pages.add',         'uses' => 'Admin\PageController@add']);
Route::post('admin/page/store',              ['as' => 'admin.pages.store',       'uses' => 'Admin\PageController@store']);
Route::get('admin/page/{id}/edit',           ['as' => 'admin.pages.edit',        'uses' => 'Admin\PageController@edit']);
Route::post('admin/page/{id}/update',        ['as' => 'admin.pages.update',      'uses' => 'Admin\PageController@update']);
Route::get('admin/page/{id}/delete',         ['as' => 'admin.pages.delete',      'uses' => 'Admin\PageController@delete']);


Route::get('admin/menues',                     ['as' => 'admin.menues.index',       'uses' => 'Admin\MenuController@index']);
Route::get('admin/menu/add',                  ['as' => 'admin.menues.add',         'uses' => 'Admin\MenuController@add']);
Route::post('admin/menu/store',               ['as' => 'admin.menues.store',       'uses' => 'Admin\MenuController@store']);
Route::get('admin/menu/{id}/edit',            ['as' => 'admin.menues.edit',        'uses' => 'Admin\MenuController@edit']);
Route::post('admin/menu/{id}/update',         ['as' => 'admin.menues.update',      'uses' => 'Admin\MenuController@update']);
Route::get('admin/menu/{id}/delete',          ['as' => 'admin.menues.delete',      'uses' => 'Admin\MenuController@delete']);

Route::get('admin/products',                        ['as' => 'admin.products.index',            'uses' => 'Admin\ProductController@index']);
Route::get('admin/product/add',                     ['as' => 'admin.products.add',              'uses' => 'Admin\ProductController@add']);
Route::post('admin/product/store',                  ['as' => 'admin.products.store',            'uses' => 'Admin\ProductController@store']);
Route::get('admin/product/{id}/edit',               ['as' => 'admin.products.edit',             'uses' => 'Admin\ProductController@edit']);
Route::post('admin/product/{id}/update',            ['as' => 'admin.products.update',           'uses' => 'Admin\ProductController@update']);
Route::get('admin/product/{id}/delete',             ['as' => 'admin.products.delete',           'uses' => 'Admin\ProductController@delete']);
Route::post('admin/product/getAttributeHtml',       ['as' => 'admin.products.getAttributeHtml', 'uses' => 'Admin\ProductController@getAttributeHtml']);
Route::post('admin/product/getaimageHtml',          ['as' => 'admin.products.getImageHtml',     'uses' => 'Admin\ProductController@getImageHtml']);

Route::get('admin/tests',                     ['as' => 'admin.tests.index',       'uses' => 'Admin\TestController@index']);
Route::get('admin/test/add',                  ['as' => 'admin.tests.add',         'uses' => 'Admin\TestController@add']);
Route::post('admin/test/store',               ['as' => 'admin.tests.store',       'uses' => 'Admin\TestController@store']);
Route::get('admin/test/{id}/edit',            ['as' => 'admin.tests.edit',        'uses' => 'Admin\TestController@edit']);
Route::post('admin/test/{id}/update',         ['as' => 'admin.tests.update',      'uses' => 'Admin\TestController@update']);
Route::get('admin/test/{id}/delete',          ['as' => 'admin.tests.delete',      'uses' => 'Admin\TestController@delete']);

//------------------this section is for CREATIVE LINK PRALI--------------------------------//

//------------------OUR SERVICES PAGE---------------------//
Route::get('admin/services',                  ['as' => 'admin.services.index',       'uses' => 'Admin\ServiceController@index']);
Route::get('admin/service/add',               ['as' => 'admin.services.add',         'uses' => 'Admin\ServiceController@add']);
Route::post('admin/service/store',            ['as' => 'admin.services.store',       'uses' => 'Admin\ServiceController@store']);
Route::get('admin/service/{id}/edit',         ['as' => 'admin.services.edit',        'uses' => 'Admin\ServiceController@edit']);
Route::post('admin/service/{id}/update',      ['as' => 'admin.services.update',      'uses' => 'Admin\ServiceController@update']);
Route::get('admin/service/{id}/delete',       ['as' => 'admin.services.delete',      'uses' => 'Admin\ServiceController@delete']);


//------------------BANNER SECTION PAGE------------------------//
Route::get('admin/banners',                                  ['as' => 'admin.banners.index',                      'uses' => 'Admin\BannerController@index']);
Route::get('admin/banner/add',                               ['as' => 'admin.banners.add',                        'uses' => 'Admin\BannerController@add']);
Route::post('admin/banner/store',                            ['as' => 'admin.banners.store',                      'uses' => 'Admin\BannerController@store']);
Route::get('admin/banner/{id}/edit',                         ['as' => 'admin.banners.edit',                       'uses' => 'Admin\BannerController@edit']);
Route::post('admin/banner/{id}/update',                      ['as' => 'admin.banners.update',                     'uses' => 'Admin\BannerController@update']);
Route::get('admin/banner/{id}/delete',                       ['as' => 'admin.banners.delete',                     'uses' => 'Admin\BannerController@delete']);

Route::get('admin/product-attributes',                       ['as' => 'admin.product-attributes.index',           'uses' => 'Admin\ProductAttributeController@index']);
Route::get('admin/product-attribute/add',                    ['as' => 'admin.product-attributes.add',             'uses' => 'Admin\ProductAttributeController@add']);
Route::post('admin/product-attribute/store',                 ['as' => 'admin.product-attributes.store',           'uses' => 'Admin\ProductAttributeController@store']);
Route::get('admin/product-attribute/{id}/edit',              ['as' => 'admin.product-attributes.edit',            'uses' => 'Admin\ProductAttributeController@edit']);
Route::post('admin/product-attribute/{id}/update',           ['as' => 'admin.product-attributes.update',          'uses' => 'Admin\ProductAttributeController@update']);
Route::get('admin/product-attribute/{id}/delete',            ['as' => 'admin.product-attributes.delete',          'uses' => 'Admin\ProductAttributeController@delete']);


Route::get('admin/attribute-groups',                        ['as' => 'admin.attribute-groups.index',              'uses' => 'Admin\AttributeGroupController@index']);
Route::get('admin/attribute-group/add',                     ['as' => 'admin.attribute-groups.add',                'uses' => 'Admin\AttributeGroupController@add']);
Route::post('admin/attribute-group/store',                  ['as' => 'admin.attribute-groups.store',              'uses' => 'Admin\AttributeGroupController@store']);
Route::get('admin/attribute-group/{id}/edit',               ['as' => 'admin.attribute-groups.edit',               'uses' => 'Admin\AttributeGroupController@edit']);
Route::post('admin/attribute-group/{id}/update',            ['as' => 'admin.attribute-groups.update',             'uses' => 'Admin\AttributeGroupController@update']);
Route::get('admin/attribute-group/{id}/delete',             ['as' => 'admin.attribute-groups.delete',             'uses' => 'Admin\AttributeGroupController@delete']);

Route::get('admin/product-tags',                            ['as' => 'admin.product-tags.index',                  'uses' => 'Admin\ProductTagController@index']);
Route::get('admin/product-tag/add',                         ['as' => 'admin.product-tags.add',                    'uses' => 'Admin\ProductTagController@add']);
Route::post('admin/product-tag/store',                      ['as' => 'admin.product-tags.store',                  'uses' => 'Admin\ProductTagController@store']);
Route::get('admin/product-tag/{id}/edit',                   ['as' => 'admin.product-tags.edit',                   'uses' => 'Admin\ProductTagController@edit']);
Route::post('admin/product-tag/{id}/update',                ['as' => 'admin.product-tags.update',                 'uses' => 'Admin\ProductTagController@update']);
Route::get('admin/product-tag/{id}/delete',                 ['as' => 'admin.product-tags.delete',                 'uses' => 'Admin\ProductTagController@delete']);

Route::get('admin/product-categorys',                       ['as' => 'admin.product-categorys.index',           'uses' => 'Admin\ProductCategoryController@index']);
Route::get('admin/product-category/add',                    ['as' => 'admin.product-categorys.add',             'uses' => 'Admin\ProductCategoryController@add']);
Route::post('admin/product-category/store',                 ['as' => 'admin.product-categorys.store',           'uses' => 'Admin\ProductCategoryController@store']);
Route::get('admin/admin/product-category/{id}/edit',        ['as' => 'admin.product-categorys.edit',            'uses' => 'Admin\ProductCategoryController@edit']);
Route::post('admin/product-category/{id}/update',           ['as' => 'admin.product-categorys.update',          'uses' => 'Admin\ProductCategoryController@update']);
Route::get('admin/product-category/{id}/delete',            ['as' => 'admin.product-categorys.delete',          'uses' => 'Admin\ProductCategoryController@delete']);
Route::post('admin/product-category/{id}/ajax',             ['as' => 'admin.product-categorys.ajax',            'uses' => 'Admin\ProductCategoryController@ajax']);
