<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected  $view_path = 'admin.user';
    protected  $base_route = 'admin.users';
    public function index(Request $request)
    {
        //mass assignment method
        $data = [];
        $data['users'] = User::select('id', 'username', 'email', 'first_name', 'middle_name', 'status','last_name', 'created_at', 'updated_at')->get();
        //dd($data['users']->count());

        /*$user = User::create($data);
        dd($user);
        dd($user->delete())
        */



        return view($this->view_path.'.list',  [
            'data'=> $data
        ]);
    }

    public function add(Request $request)
    {
       return view($this->view_path.'.add');
    }

    public function store(Request $request)
    {
        //print all the data comming from request
        //$request->all(); or dd($request->all())
       // dd($request->all());
        //method one to insert data into database
        /*$user = new User();
        $user->username = $request->get('username');
        $user->email = $request->get('email');
        $user->first_name = $request->get('first_name');
        $user->middle_name = $request->get('middle_name');
        $user->last_name = $request->get('last_name');
        $user->password = bcrypt($request->get('passowrd'));
        $user->status = $request->get('status');

        $user->save();*/

        User::create([
           'username'  => $request->get('username'),
           'email'  => $request->get('email'),
           'first_name'  => $request->get('first_name'),
           'middle_name'  => $request->get('middle_name'),
           'last_name'  => $request->get('last_name'),
           'password'  => $request->get('password'),
           'status'  => $request->get('status'),
        ]);

        $request->session()->flash('message', 'User Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        $data['user'] = User::find($id);

           // dd($data); -> sabai data taneko ab view ma pathaunu parxa



        return view($this->view_path.'.edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        //check user data for $id
        //dd($request->all());

        $user = User::find($id);

        //dd($user); instance leko edit garne data ko lagi

        $user->update([
                'username'  => $request->get('username'),
                'email'  => $request->get('email'),
                'password' => $request->has('password')?bcrypt($request->get('password')):$user->password ,
                'first_name'  => $request->get('first_name'),
                'middle_name'  => $request->get('middle_name'),
                'last_name'  => $request->get('last_name'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'User Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }



    public function delete(Request $request, $id)
    {
        //get user data for $id and remove
        User::find($id)->delete();
        $request->session()->flash('message', 'User Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }

}