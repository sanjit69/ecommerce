<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use DB, File, View;

class AdminBaseController extends Controller
{


    protected function checkFolderExist()
    {
        if (!File::exists($this->folder_path)){
            //create folder
            File::makeDirectory($this->folder_path);

        }
    }

    protected function loadDefaultVars($path)
    {
        View::composer($path, function ($view){
            $view->with('base_route', $this->base_route);
            $view->with('view_path', $this->view_path.'.');
            $view->with('trans_path', $this->getTransPath());
        });

        return $path;
    }
    protected function getTransPath()
    {
        $view_path_array = explode('.', $this->view_path);
        $trans_path = implode('/', $view_path_array).'/general.';
        return $trans_path;
        //dd($trans_path);
    }

}