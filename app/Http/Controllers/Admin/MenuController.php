<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Menu\AddMenuValidation;
use ViewHelper;

use App\Http\Requests\Page\PageAddValidation;
use App\Http\Requests\Page\PageEditValidation;
use App\Models\Menu;
use App\Models\Page;
use App\User;
use DB, File;
use Illuminate\Http\Request;

class MenuController extends AdminBaseController
{
    protected  $view_path = 'admin.menu';
    protected  $base_route = 'admin.menues';
    protected $folder_name = 'menu';
    protected $folder_path;

    public  function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {

        $data = [];

        $data['rows'] = Menu::select('id', 'title', 'status', 'created_at')->get();

        $data['trans_path'] = $this->getTransPath();
        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        $data['trans_path'] = $this->getTransPath();
       return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(AddMenuValidation $request)
    {
        Menu::create([
           'title'  => $request->get('title'),
           'key'  => $request->get('key'),
           'description'  => $request->get('description'),
           'status'  => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Menu Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        if (!$data['row'] = Menu::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //dd($data);
        $data['menu'] = Menu::select('id', 'title')->orderBy('title', 'asc')->get();
        $data['trans_path'] = $this->getTransPath();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PageEditValidation $request, $id)
    {

        //dd($request->all());
        if (!$menu = Menu::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

            $menu->update([
                'title'  => $request->get('title'),
                'key'  => $request->get('key'),
                'description'  => $request->get('description'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'Menu Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }



    public function delete(Request $request, $id)
    {
        if (!$page = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //remove image before deleting db row
        if ($page->banner) {
            //if old image is exist remove old image
            if (File::exists($this->folder_path.$page->banner)){
                File::delete($this->folder_path.$page->banner);
            }

        }
        $page->delete();
        $request->session()->flash('message', 'Page Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }

}