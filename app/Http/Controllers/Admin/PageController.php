<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use ViewHelper;

use App\Http\Requests\Page\PageAddValidation;
use App\Http\Requests\Page\PageEditValidation;
use App\Models\Menu;
use App\Models\Page;
use App\User;
use DB, File;
use Illuminate\Http\Request;

class PageController extends AdminBaseController
{
    protected  $view_path = 'admin.page';
    protected  $base_route = 'admin.pages';
    protected $folder_name = 'page';
    protected $folder_path;

    public  function __construct()
    {
        //Parent::__construct();
        //public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name;
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {

        $data = [];
        //$data['rows'] = Page::OrderBy('id', 'desc')->get(); or
       /* $data['rows'] = Page::select('id', 'menu_id', 'title', 'created_at', 'updated_at', 'status')
            ->orderBy('id', 'desc')
            ->get();  */

        $data['rows'] = Page::select('pages.id', 'm.title as menu_title', 'pages.title as page_title', 'pages.created_at', 'pages.updated_at', 'pages.status')
            ->leftJoin('menu as m', 'm.id', '=', 'pages.menu_id')
            ->orderBy('pages.id', 'desc')
            ->get();


        //dd($data);
        //$data['view_path'] = $this->view_path.'.';
        $data['trans_path'] = $this->getTransPath();
        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];
        $data['menu'] = Menu::select('id', 'title')->orderBy('title', 'asc')->get();
        //$data['view_path'] = $this->view_path.'.';
        //$data['trans_path'] = $this->getTransPath();
       return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(PageAddValidation $request)
    {
        //dd($request->all());
        //dd($request->files);

        if ($request->hasFile('banner'))
        {
            //dd($request->file('banner'));
            //dd($this->folder_path);
            //check if the folder exist
            //if not exist create folder
            Parent::checkFolderExist();

            $image = $request->file('banner');
            $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $image_name);

            //dd('not exist');
        }
        Page::create([
           'menu_id'  => $request->get('menu_id'),
           'title'  => $request->get('title'),
           'slug'  => str_slug($request->get('title')),
           'banner'  => isset($image_name)?$image_name:'',
           'description'  => $request->get('description'),
           'status'  => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Page Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        if (!$data['row'] = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //dd($data);
        $data['menu'] = Menu::select('id', 'title')->orderBy('title', 'asc')->get();
        //$data['view_path'] = $this->view_path.'.';
        //$data['trans_path'] = $this->getTransPath();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PageEditValidation $request, $id)
    {

        //dd($request->all());
        if (!$page = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

            if ($request->hasFile('banner')) {
                parent::checkFolderExist();

                if ($page->banner) {
                    //if old image is exist remove old image
                    if (File::exists($this->folder_path.$page->banner)){
                        File::delete($this->folder_path.$page->banner);
                    }

                }

                $image = $request->file('banner');
                $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
                $image->move($this->folder_path, $image_name);

            }

            $page->update([
                'menu_id'  => $request->get('menu_id'),
                'title'  => $request->get('title'),
                'slug'  => str_slug($request->get('title')),
                'banner' => isset($image_name)?$image_name:$page->banner,
                'description'  => $request->get('description'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'Page Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }



    public function delete(Request $request, $id)
    {
        if (!$page = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //remove image before deleting db row
        if ($page->banner) {
            //if old image is exist remove old image
            if (File::exists($this->folder_path.$page->banner)){
                File::delete($this->folder_path.$page->banner);
            }

        }
        $page->delete();
        $request->session()->flash('message', 'Page Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }

}