<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

//use App\Models\Page;
use App\Models\Service;
use DB, File;
use Illuminate\Http\Request;

class ServiceController extends AdminBaseController
{
    protected  $view_path = 'admin.service';
    protected  $base_route = 'admin.services';
    protected $folder_name = 'service';
    protected $folder_path;

    public  function __construct()
    {
        //Parent::__construct();
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
    }

    public function index(Request $request)
    {
        //dd('index controller');

        $data = [];
        $data['rows'] = Service::select('title', 'sub_title', 'status', 'created_at', 'created_by')->get();

        return view($this->view_path.'.list', compact('data'));
    }

    public function add(Request $request)
    {
        $data = [];

       return view($this->view_path.'.add', compact('data'));
    }

    public function store(Request $request)
    {
        //dd($request->all());
        //dd($request->files);

        if ($request->hasFile('image'))
        {
            //dd($request->file('banner'));
            //dd($this->folder_path);
            //check if the folder exist
            //if not exist create folder
            Parent::checkFolderExist();

            $image = $request->file('image');
            $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $image_name);

            //dd('not exist');
        }
        Service::create([

           'title'  => $request->get('title'),
           'sub_title'  => str_slug($request->get('title')),
           'image'  => isset($image_name)?$image_name:'',
           'description'  => $request->get('description'),
           'other_description'  => $request->get('other_description'),
           'status'  => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Service Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        if (!$data['row'] = Page::find($id)){
            dd('invalid request');
        }

        //dd($data);
        $data['menu'] = Menu::select('id', 'title')->orderBy('title', 'asc')->get();

        return view($this->view_path.'.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {

        if (!$page = Page::find($id))
            dd('Invalid Request');

            if ($request->hasFile('banner')) {
                parent::checkFolderExist();

                if ($page->banner) {
                    //if old image is exist remove old image
                    if (File::exists($this->folder_path.$page->banner)){
                        File::delete($this->folder_path.$page->banner);
                    }

                }

                $image = $request->file('banner');
                $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
                $image->move($this->folder_path, $image_name);

            }

            $page->update([
                'menu_id'  => $request->get('menu_id'),
                'title'  => $request->get('title'),
                'slug'  => str_slug($request->get('title')),
                'banner' => isset($image_name)?$image_name:$page->banner,
                'description'  => $request->get('description'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'Page Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }



    public function delete(Request $request, $id)
    {
        if (!$page = Page::find($id))
            dd('invalid request');

        //remove image before deleting db row
        if ($page->banner) {
            //if old image is exist remove old image
            if (File::exists($this->folder_path.$page->banner)){
                File::delete($this->folder_path.$page->banner);
            }

        }

        $request->session()->flash('message', 'Page Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }

}