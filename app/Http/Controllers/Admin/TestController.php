<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Http\Requests\Menu\AddMenuValidation;
use App\Models\Menu;
use Illuminate\Http\Request;

class TestController extends Controller
{
    protected  $view_path = 'admin.test';
    protected  $base_route = 'admin.tests';

    public function index(Request $request)
    {
        //dd('dsd');

        $data = [];
        $data['rows'] = Menu::select('id', 'title', 'description', 'status', 'created_at', 'updated_at')->get();



        return view($this->view_path.'.list', compact('data'));
    }

    public function add(Request $request)
    {
       return view($this->view_path.'.add');
    }

    public function store(AddMenuValidation $request)
    {

        Menu::create([
           'title'  => $request->get('title'),
           'key'  => $request->get('key'),
           'description'  => $request->get('description'),
            'status' => $request->get('status'),
        ]);

        $request->session()->flash('message', 'Menu Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        $data['user'] = User::find($id);

           // dd($data); -> sabai data taneko ab view ma pathaunu parxa



        return view($this->view_path.'.edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, $id)
    {
        //check user data for $id
        //dd($request->all());

        $user = User::find($id);

        //dd($user); instance leko edit garne data ko lagi

        $user->update([
                'username'  => $request->get('username'),
                'email'  => $request->get('email'),
                'password' => $request->has('password')?bcrypt($request->get('password')):$user->password ,
                'first_name'  => $request->get('first_name'),
                'middle_name'  => $request->get('middle_name'),
                'last_name'  => $request->get('last_name'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'User Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }



    public function delete(Request $request, $id)
    {
        //get user data for $id and remove
        User::find($id)->delete();
        $request->session()->flash('message', 'User Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }

}