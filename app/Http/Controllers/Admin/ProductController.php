<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use App\Models\AttributeGroup;
use App\Models\Product;
use App\Models\ProductAttribute;
use App\Models\ProductAttributeSpecification;
use App\Models\ProductCategory;
use App\Models\ProductImages;
use App\Models\ProductTag;
use ViewHelper;
use Image;

use App\Http\Requests\Page\PageAddValidation;
use App\Http\Requests\Page\PageEditValidation;
use App\User;
use DB, File;
use Illuminate\Http\Request;

class ProductController extends AdminBaseController
{
    protected  $view_path = 'admin.product';
    protected  $base_route = 'admin.products';
    protected $folder_name = 'product';
    protected $folder_path;
    protected $main_image_dimentions;
    protected $gallery_image_dimentions;

    public  function __construct()
    {
        $this->folder_path = public_path().DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.$this->folder_name.DIRECTORY_SEPARATOR;
        $this->main_image_dimentions = config('broadway.image.thumb-dimensions.product.main_image');
        $this->gallery_image_dimentions = config('broadway.image.thumb-dimensions.product.gallery_image');
    }

    public function index(Request $request)
    {
        $data = [];
        $data['rows'] = Product::select('product.id', 'c.title as category_title', 'product.name',
            'product.created_at', 'product.updated_at', 'product.slug','product.old_price', 'product.new_price',
            'product.quantity', 'product.main_image','product.status', 'product.is_featured')
            ->leftJoin('category as c', 'c.id', '=', 'product.category_id')
            ->orderBy('product.id', 'desc')
            ->get();
        return view(parent::loadDefaultVars($this->view_path.'.list'), compact('data'));

    }

    public function add(Request $request)
    {
        $data = [];
        $data['categories'] = ProductCategory::select('id', 'title')->orderBy('title', 'asc')->get();
        $data['ptags'] = ProductTag::select('id','title')->orderBy('title', 'asc')->get();
       return view(parent::loadDefaultVars($this->view_path.'.add'), compact('data'));
    }

    public function store(Request $request)
    {

        if ($request->hasFile('main_image'))
        {
            Parent::checkFolderExist();

            $image = $request->file('main_image');
            $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
            $image->move($this->folder_path, $image_name);

            foreach ($this->main_image_dimentions as $dimention) {
                $thumb_image = Image::make($this->folder_path.$image_name)->resize($dimention['width'], $dimention['height']);
                $thumb_image->save($this->folder_path.$dimention['width'].'-'.$dimention['height'].'-'.$image_name);
            }



            //dd('not exist');
        }
        $product = Product::create([
            'category_id'         =>     $request->get('category_id'),
            'name'                =>     $request->get('name'),
            'slug'                =>     str_slug($request->get('name')),
            'old_price'           =>     $request->get('old_price'),
            'new_price'           =>     $request->get('new_price') == ''?$request->get('old_price'):$request->get('new_price'),
            'quantity'            =>     $request->get('quantity'),
            'short_desc'          =>     $request->get('short_desc'),
            'long_desc'           =>     $request->get('long_desc'),
            'main_image'          =>     isset($image_name)?$image_name:'',
            'status'              =>     $request->get('status'),
            'seo_title'           =>     $request->get('seo_title'),
            'seo_desc'            =>     $request->get('seo_desc'),
            'seo_keyword'         =>     $request->get('seo_keyword'),
            'view_count'          =>     '0',
            //'created_by'          =>    auth()->user()->id,
        ]);

        //store image
        if ($request->hasFile('gallery_image')) {

            foreach($request->file('gallery_image') as $key => $image) {

                // store image in folder
                $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
                $image->move($this->folder_path, $image_name);

                foreach ($this->gallery_image_dimentions as $dimention) {
                    $thumb_image = Image::make($this->folder_path.$image_name)->resize($dimention['width'], $dimention['height']);
                    $thumb_image->save($this->folder_path.$dimention['width'].'-'.$dimention['height'].'-'.$image_name);
                }

                $data = [
                    //'created_by' => auth()->user()->id,
                    'product_id' => $product->id,
                    'image' => $image_name,
                    'caption' => $request->get('gallery_image_caption')[$key],
                    'alt_text' => $request->get('gallery_image_alt_text')[$key],
                    'rank' => $request->get('gallery_image_rank')[$key],
                    'status' => $request->get('gallery_image_status')[$key],
                ];

                ProductImages::create($data);
            }
        }

        //store Attribute Specification
        if ($request->has('value')) {

            foreach ($request->get('value') as $key => $value) {

                $data = [
                    //'created_by' => auth()->user()->id,
                    'product_id' => $product->id,
                    'product-attribute-group_id' => $request->get('attribute_group')[$key],
                    'product-attribute_id' => $request->get('attribute')[$key],
                    'value' => $value,
                    'status'=> 1,
                ];
                ProductAttributeSpecification::create($data);

            }

        }
        // Product Tag
        $product->ProductTag()->sync($request->has('tag')?$request->get('tag'):[]);
        $request->session()->flash('message', 'Product Added Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function edit(Request $request, $id)
    {
        //get the data for edit
        $data = [];
        if (!$data['row'] = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //dd($data);
        $data['menu'] = Menu::select('id', 'title')->orderBy('title', 'asc')->get();
        //$data['view_path'] = $this->view_path.'.';
        //$data['trans_path'] = $this->getTransPath();
        return view(parent::loadDefaultVars($this->view_path.'.edit'), compact('data'));
    }

    public function update(PageEditValidation $request, $id)
    {

        //dd($request->all());
        if (!$page = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

            if ($request->hasFile('banner')) {
                parent::checkFolderExist();

                if ($page->banner) {
                    //if old image is exist remove old image
                    if (File::exists($this->folder_path.$page->banner)){
                        File::delete($this->folder_path.$page->banner);
                    }

                }

                $image = $request->file('banner');
                $image_name = rand(4747, 9879).'_'.$image->getClientOriginalName();
                $image->move($this->folder_path, $image_name);

            }

            $page->update([
                'menu_id'  => $request->get('menu_id'),
                'title'  => $request->get('title'),
                'slug'  => str_slug($request->get('title')),
                'banner' => isset($image_name)?$image_name:$page->banner,
                'description'  => $request->get('description'),
                'status'  => $request->get('status'),
            ]);

        $request->session()->flash('message', 'Page Update Successfully');
        return redirect()->route($this->base_route.'.index');
    }

    public function delete(Request $request, $id)
    {
        if (!$page = Page::find($id))
            return redirect()->route('admin.error', ['code'=> '500']);

        //remove image before deleting db row
        if ($page->banner) {
            //if old image is exist remove old image
            if (File::exists($this->folder_path.$page->banner)){
                File::delete($this->folder_path.$page->banner);
            }

        }
        $page->delete();
        $request->session()->flash('message', 'Page Deleted Successfully');
        return redirect()->route($this->base_route.'.index');
    }


    public function getAttributeHtml(Request $request)
    {
        $data = [];
        $data['html'] = view($this->view_path.'.partials.add.attribute-row', [
            'attribute_groups' => AttributeGroup::select('id','title')->orderBy('title')->get(),
            'attributes' => ProductAttribute::select('id','title')->orderBy('title')->get(),
        ])->render();

        return response()->json(json_encode($data));
    }

    public function getImageHtml(Request $request)
    {
        $data = [];
        $data['html'] = view($this->view_path.'.partials.add.image-row')->render();

        return response()->json(json_encode($data));
    }

}