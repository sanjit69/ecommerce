<?php
/**
 * Created by PhpStorm.
 * User: sanju
 * Date: 12/5/16
 * Time: 10:59 PM
 */

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;

use App\User;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function index(Request $request)
    {
        //dd('this is banner controller');
        return view('admin.banner.list');
    }

    public function add(Request $request)
    {

        dd('this is banner add form');
       return view($this->view_path.'.add');
    }

    public function store(Request $request)
    {

    }

    public function edit(Request $request, $id)
    {

    }

    public function update(Request $request, $id)
    {

    }



    public function delete(Request $request, $id)
    {

    }

}