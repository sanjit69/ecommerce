<?php

namespace App\Http\Controllers;

use MyHelper, ViewHelper;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //dd(ViewHelper::test());

        //dd(MyHelper::test());

        return view('home');
    }
}
