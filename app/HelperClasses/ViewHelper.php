<?php
namespace App\HelperClasses;

class ViewHelper
{
    public function getData($key, $data = [])
    {
        if (old($key))
            return old($key);

            elseif(count($data) > 0)
                return $data->$key;
        else
            return '';
    }
}