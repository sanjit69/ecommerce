<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    //protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['created_by', 'updated_by', 'created_at', 'updated_at', 'category_id', 'name', 'slug', 'old_price', 'new_price','quantity',
        'short_desc', 'long_desc', 'main_image', 'seo_title', 'seo_description', 'seo_keyword', 'status', 'view_count'];


    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id');
    }

    public function ProductTag()
    {
        return $this->belongsToMany('App\Models\ProductTag', 'product-product-tag', 'product_id', 'tag_id');
    }

    public function productImage()
    {
        return $this->hasMany('App\Models\ProductImages', 'product_id');
    }
    public function productAttributeSpec()
    {
        return $this->hasMany('App\Models\ProductAttributeSpecification', 'product_id');
    }
}
