<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';


    protected $guarded = [];

    protected $fillable = ['id', 'created_by', 'updated_by', 'created_at', 'updated_at', 'title', 'sub_title', 'image', 'description', 'other_description', 'status'];


    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id');
    }
}
