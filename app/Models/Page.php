<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $table = 'pages';

    //protected $timestamp = false;

    protected $guarded = [];

    protected $fillable = ['created_by', 'updated_by', 'menu_id', 'title', 'slug', 'status', 'banner', 'description'];


    public function menu()
    {
        return $this->belongsTo('App\Models\Menu', 'menu_id');
    }
}
